﻿Unit Unit1;

interface

uses System, System.Drawing, System.Windows.Forms;

type
  Form1 = class(Form)
    procedure textBox4_TextChanged(sender: Object; e: EventArgs);
    procedure label4_Click(sender: Object; e: EventArgs);
    procedure button1_Click(sender: Object; e: EventArgs);
  {$region FormDesigner}
  internal
    {$resource Unit1.Form1.resources}
    button1: Button;
    textBox2: TextBox;
    textBox3: TextBox;
    label1: &Label;
    label2: &Label;
    label3: &Label;
    label4: &Label;
    textBox1: TextBox;
    label5: &Label;
    {$include Unit1.Form1.inc}
  {$endregion FormDesigner}
  public
    constructor;
    begin
      InitializeComponent;
    end;
  end;

implementation

procedure Form1.textBox4_TextChanged(sender: Object; e: EventArgs);
begin
  
end;

procedure Form1.label4_Click(sender: Object; e: EventArgs);
begin
  
end;

function ModExp(base, exponent, modulus: integer):integer;

begin
  result := 1;
  while exponent > 0 do
  begin
    if exponent mod 2 = 1 then
      result := (result * base) mod modulus;
    base := (base * base) mod modulus;
    exponent := exponent div 2;
  end;
  ModExp := result;
end;


procedure Form1.button1_Click(sender: Object; e: EventArgs);
 var base, exponent, modulus: integer;
begin
  try
  base:=StrToInt(Textbox1.text);
  exponent:=StrToInt(Textbox2.Text);
  modulus:=StrToInt(Textbox3.Text);
  label5.text:=inttostr(ModExp(base, exponent, modulus));
  except
  label4.Text:=' Вы дурак';  
  end;
  
end;

end.
