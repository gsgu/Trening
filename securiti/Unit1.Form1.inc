procedure InitializeComponent;
    begin
        self.button1 := new System.Windows.Forms.Button();
        self.textBox2 := new System.Windows.Forms.TextBox();
        self.textBox3 := new System.Windows.Forms.TextBox();
        self.label1 := new System.Windows.Forms.Label();
        self.label2 := new System.Windows.Forms.Label();
        self.label3 := new System.Windows.Forms.Label();
        self.label4 := new System.Windows.Forms.Label();
        self.textBox1 := new System.Windows.Forms.TextBox();
        self.label5 := new System.Windows.Forms.Label();
        self.SuspendLayout();
        // 
        // button1
        // 
        self.button1.Location := new System.Drawing.Point(188, 213);
        self.button1.Name := 'button1';
        self.button1.Size := new System.Drawing.Size(99, 23);
        self.button1.TabIndex := 0;
        self.button1.Text := 'Посчитать';
        self.button1.UseVisualStyleBackColor := true;
        self.button1.Click += button1_Click;
        // 
        // textBox2
        // 
        self.textBox2.Location := new System.Drawing.Point(84, 106);
        self.textBox2.Name := 'textBox2';
        self.textBox2.Size := new System.Drawing.Size(303, 20);
        self.textBox2.TabIndex := 2;
        // 
        // textBox3
        // 
        self.textBox3.Location := new System.Drawing.Point(84, 169);
        self.textBox3.Name := 'textBox3';
        self.textBox3.Size := new System.Drawing.Size(303, 20);
        self.textBox3.TabIndex := 3;
        // 
        // label1
        // 
        self.label1.Location := new System.Drawing.Point(84, 80);
        self.label1.Name := 'label1';
        self.label1.Size := new System.Drawing.Size(100, 23);
        self.label1.TabIndex := 5;
        self.label1.Text := 'Введите степень';
        // 
        // label2
        // 
        self.label2.Location := new System.Drawing.Point(84, 9);
        self.label2.Name := 'label2';
        self.label2.Size := new System.Drawing.Size(119, 23);
        self.label2.TabIndex := 6;
        self.label2.Text := 'Введите основание ';
        // 
        // label3
        // 
        self.label3.Location := new System.Drawing.Point(84, 143);
        self.label3.Name := 'label3';
        self.label3.Size := new System.Drawing.Size(100, 23);
        self.label3.TabIndex := 7;
        self.label3.Text := 'Введите модуль ';
        // 
        // label4
        // 
        self.label4.Location := new System.Drawing.Point(84, 252);
        self.label4.Name := 'label4';
        self.label4.Size := new System.Drawing.Size(134, 23);
        self.label4.TabIndex := 8;
        self.label4.Text := 'Ответ:';
        self.label4.Click += label4_Click;
        // 
        // textBox1
        // 
        self.textBox1.Location := new System.Drawing.Point(84, 35);
        self.textBox1.Name := 'textBox1';
        self.textBox1.Size := new System.Drawing.Size(303, 20);
        self.textBox1.TabIndex := 9;
        // 
        // label5
        // 
        self.label5.Location := new System.Drawing.Point(158, 252);
        self.label5.Name := 'label5';
        self.label5.Size := new System.Drawing.Size(161, 23);
        self.label5.TabIndex := 10;
        // 
        // Form1
        // 
        self.ClientSize := new System.Drawing.Size(727, 508);
        self.Controls.Add(self.label5);
        self.Controls.Add(self.textBox1);
        self.Controls.Add(self.label4);
        self.Controls.Add(self.label3);
        self.Controls.Add(self.label2);
        self.Controls.Add(self.label1);
        self.Controls.Add(self.textBox3);
        self.Controls.Add(self.textBox2);
        self.Controls.Add(self.button1);
        self.Name := 'Form1';
        self.Text := 'Form1';
        self.ResumeLayout(false);
        self.PerformLayout();
    end;
